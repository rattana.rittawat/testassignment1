package com.example.rattana.testassignment.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import coil.api.load
import com.example.rattana.testassignment.R
import com.example.rattana.testassignment.model.News
import kotlinx.android.synthetic.main.activity_detail_news.*


class DetailNewsActivity : AppCompatActivity() {

    //lateinit var mPresent : MainPresenter

    lateinit var mNewsFeedList: News

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_news)

        val i = intent
        val image = i.getStringExtra("imageView")
        val title = i.getStringExtra("title")
        val detail = i.getStringExtra("detail")

        imgNews.load(image)
        txtTitle.text = title
        txtDetail.text = detail

        //setOnClick()

        //setupMVP()
        //setupViews()
        //getNewsDetail()

    }

    private fun setOnClick() {
        txtLike.setOnClickListener {
            /*if(mNewsFeedList.isLike!!){
                txtLike.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(this , R.mipmap.ic_like_active)
                    , null
                    , null
                    , null)
            }else{
                txtLike.setCompoundDrawablesWithIntrinsicBounds(
                    ContextCompat.getDrawable(this , R.mipmap.ic_like)
                    , null
                    , null
                    , null)
            }
            txtLike.text = mNewsFeedList.countLike.toString()*/
        }
    }

}
