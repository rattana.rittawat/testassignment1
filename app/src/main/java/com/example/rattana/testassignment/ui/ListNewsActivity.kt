package com.example.rattana.testassignment.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rattana.testassignment.R
import com.example.rattana.testassignment.adapter.ListNewsAdapter
import com.example.rattana.testassignment.model.News
import kotlinx.android.synthetic.main.activity_list_news.*
import android.util.Log
import com.example.rattana.testassignment.model.ListNewsResponse


class ListNewsActivity : AppCompatActivity(), MainView {

    //private lateinit var rcvNewsFeed: RecyclerView
    lateinit var mPresent : MainPresenter
    private var mAdapter: ListNewsAdapter? = null
    private val mNewsFeedList = ArrayList<News>()

    companion object{
        val INTENT_PARCELABLE = "OBJECT_INTENT"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_news)

        setupMVP()
        setupViews()
        getNewsList()
        //setAdapter()

        /*addNewsFeedModel(
            "1",
            "42dadd4c-6c63-41d8-933a-feed678b0c11",
            1543921874,
            "Terrace",
            "https://s3.amazonaws.com/uifaces/faces/twitter/baumann_alex/128.jpg",
            "1 . Morbi sagittis blandit diam, id lacinia eros ullamcorper et. Praesent mollis, ante nec tempus laoreet, arcu metus ultrices turpis, blandit fermentum tortor sapien ut mi. Fusce tincidunt metus lacus, vel tristique dolor finibus in. Donec efficitur pellentesque orci."
        )

        addNewsFeedModel(
            "2",
            "e24c9234-696f-408b-bb68-71854b8f603e",
            1543921814,
            "Specialist bleeding-edge Glen",
            "https://s3.amazonaws.com/uifaces/faces/twitter/dgclegg/128.jpg",
            "2 . Morbi sagittis blandit diam, id lacinia eros ullamcorper et. Praesent mollis, ante nec tempus laoreet, arcu metus ultrices turpis, blandit fermentum tortor sapien ut mi. Fusce tincidunt metus lacus, vel tristique dolor finibus in. Donec efficitur pellentesque orci."
        )

        addNewsFeedModel(
            "3",
            "1b3b92bc-472b-4df0-94b2-2ba641723421",
            1543921754,
            "protocol Legacy",
            "https://s3.amazonaws.com/uifaces/faces/twitter/prheemo/128.jpg",
            "3 . Morbi sagittis blandit diam, id lacinia eros ullamcorper et. Praesent mollis, ante nec tempus laoreet, arcu metus ultrices turpis, blandit fermentum tortor sapien ut mi. Fusce tincidunt metus lacus, vel tristique dolor finibus in. Donec efficitur pellentesque orci."
        )

        addNewsFeedModel(
            "4",
            "8170bfdc-9c70-4b28-b977-aa58fc18c6db",
            1543921694,
            "Credit Card Account",
            "https://s3.amazonaws.com/uifaces/faces/twitter/rickdt/128.jpg",
            "4 . Morbi sagittis blandit diam, id lacinia eros ullamcorper et. Praesent mollis, ante nec tempus laoreet, arcu metus ultrices turpis, blandit fermentum tortor sapien ut mi. Fusce tincidunt metus lacus, vel tristique dolor finibus in. Donec efficitur pellentesque orci."
        )

        addNewsFeedModel(
            "5",
            "ad185f2e-eceb-43a6-8b95-84636fb5893f",
            1543921634,
            "Down-sized calculating Tunnel",
            "https://s3.amazonaws.com/uifaces/faces/twitter/iamkeithmason/128.jpg",
            "5 . Morbi sagittis blandit diam, id lacinia eros ullamcorper et. Praesent mollis, ante nec tempus laoreet, arcu metus ultrices turpis, blandit fermentum tortor sapien ut mi. Fusce tincidunt metus lacus, vel tristique dolor finibus in. Donec efficitur pellentesque orci."
        )*/
    }

    private fun setupMVP() {
        mPresent = MainPresenter(this)
    }

    private fun setupViews() {
        val layoutManager = LinearLayoutManager(this@ListNewsActivity)
        rcvNewsFeed.layoutManager = layoutManager
    }

    private fun getNewsList() {
            mPresent.getNews()
    }

    private fun setInitView() {

    }

    private fun setAdapter() {
        /*mAdapter = ListNewsAdapter(this,mNewsFeedList)
        val layoutManager = LinearLayoutManager(this@ListNewsActivity)
        rcvNewsFeed.layoutManager = layoutManager
        rcvNewsFeed.adapter = mAdapterg
        mAdapter?.notifyDataSetChanged()*/
    }

    override fun showToast(s: String) {
        Toast.makeText(this,s,Toast.LENGTH_LONG).show()
    }

    override fun displayNews(itemList: ListNewsResponse) {
        Log.d("displayNews >> ", "${itemList.data}")
        mAdapter = ListNewsAdapter(this, itemList.data)
        rcvNewsFeed.adapter = mAdapter
    }

    override fun displayError(s: String) {
        showToast(s)
    }

    /*private fun addNewsFeedModel(
        id: String,
        uuid: String,
        create: Int,
        title: String,
        image: String,
        detail: String
    ) {
        val temp = News(id, uuid, create, title, image, detail)
        mNewsFeedList.add(temp)
    }*/
}
