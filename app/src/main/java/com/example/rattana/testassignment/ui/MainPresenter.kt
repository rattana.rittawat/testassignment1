package com.example.rattana.testassignment.ui

import androidx.lifecycle.MutableLiveData
import com.example.rattana.testassignment.model.ListNewsResponse
import com.example.rattana.testassignment.service.ServiceAPI
import com.example.rattana.testassignment.service.ServiceClient
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers



class MainPresenter(private val view: MainView) : MainPresenterInterface {
    override fun getNews() {
        getObservable().subscribeWith(getObserver())
    }

    private fun getObservable(): Observable<ListNewsResponse> {
        return ServiceClient.client()!!.create(ServiceAPI::class.java)
            .news()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    private fun getObserver() : DisposableObserver<ListNewsResponse> = object : DisposableObserver<ListNewsResponse>(){
        override fun onComplete() {

        }

        override fun onNext(itemList:ListNewsResponse) {
            view.displayNews(itemList)
        }

        override fun onError(e: Throwable) {
            e.printStackTrace()
            view.displayError("Error fetching Movie Data")
        }

    }

    /*fun getNews(){
        ServiceClient.client.create(ServiceAPI::class.java)
            .news()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: Observer<ListNewsResponse> {
                override fun onComplete() {}

                override fun onSubscribe(d: Disposable) {}

                override fun onNext(item: ListNewsResponse) {
                    //view.setAdapterData(item.data)
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                }

            })
    }*/
}

interface MainPresenterInterface {
    fun getNews()
}