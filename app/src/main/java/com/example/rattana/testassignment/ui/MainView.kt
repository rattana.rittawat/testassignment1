package com.example.rattana.testassignment.ui

import com.example.rattana.testassignment.model.ListNewsResponse
import com.example.rattana.testassignment.model.News


interface MainView {
    //fun setAdapterData(data:ArrayList<News>)
    fun showToast(s: String)
    fun displayNews(itemList:ListNewsResponse)
    fun displayError(s: String)
}