package com.example.rattana.testassignment.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.rattana.testassignment.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    //lateinit var sharedPref : SharedPreferencesLogin
    var counter: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setOnClick()
    }
    private fun setOnClick() {
        btnLogin.setOnClickListener {
            val userName = txtUser.text.toString()
            val userPassword = txtPassword.text.toString()
            validate(userName,userPassword)
        }
    }
    //userName:String, userPassword:String
    fun validate(userName:String, userPassword:String){
        if((userName == "usertest") && (userPassword == "1234")){
            val intent = Intent(applicationContext, ListNewsActivity::class.java)
            startActivity(intent)
            Toast.makeText(this, "คุณได้เข้าสู่ระบบ", Toast.LENGTH_SHORT).show()
        }else{
            if (counter == 0){
                btnLogin.isEnabled = false
            }
            Toast.makeText(this, "คูณเข้าระบบไม่สำเร็จ โปรดลองอีกครั้ง", Toast.LENGTH_SHORT).show()
        }
    }
}
