package com.example.rattana.testassignment.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.rattana.testassignment.R
import com.example.rattana.testassignment.model.News
import com.example.rattana.testassignment.ui.DetailNewsActivity
import kotlinx.android.synthetic.main.list_item.view.*


class ListNewsAdapter(private val mContext: Context?
                      , private val mNewsFeedList: ArrayList<News>
) : RecyclerView.Adapter<ListNewsAdapter.ViewHolder>() {


    @SuppressLint("InflateParams")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemLayoutView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, null)
        itemLayoutView.layoutParams = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)

        return ViewHolder(itemLayoutView)    }

    override fun getItemCount(): Int = mNewsFeedList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtNameNews.text = mNewsFeedList[position].title
        Log.d("text News>> ", "$mNewsFeedList")
        holder.rootConstraint.setOnClickListener {
            /*val showNews = Intent(mContext,ShowNewsActivity::class.java)
            mContext?.startActivity(showNews)*/
            val i = Intent(mContext, DetailNewsActivity::class.java)
            i.putExtra("imageView", mNewsFeedList[position].image)
            i.putExtra("title", mNewsFeedList[position].title)
            i.putExtra("detail", mNewsFeedList[position].detail)
            mContext?.startActivity(i)
        }
        mContext?.let {
            Glide.with(it).load(mNewsFeedList[position].image).into(holder.imgNews)
        }
    }

    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        val rootConstraint: ConstraintLayout = itemLayoutView.rootConstraint
        val txtNameNews: TextView = itemLayoutView.txtNameNews
        val imgNews: ImageView = itemLayoutView.imgNews
    }
}