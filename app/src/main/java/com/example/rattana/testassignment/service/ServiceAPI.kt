package com.example.rattana.testassignment.service

import com.example.rattana.testassignment.model.ListNewsResponse
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ServiceAPI {
    @GET("news")
    fun news(): Observable<ListNewsResponse>

    @POST("login")
    fun login(): Observable<ListNewsResponse>

    //@Query("api_key") api_key:String
}