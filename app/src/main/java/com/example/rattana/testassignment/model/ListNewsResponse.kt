package com.example.rattana.testassignment.model

import com.google.gson.annotations.SerializedName

data class ListNewsResponse(
    @SerializedName("status") val status: Int, //200
    @SerializedName("message") val message: String, //"success"
    @SerializedName("data") val data: ArrayList<News>
)

data class News(
    @SerializedName("id")
    var id: String,
    @SerializedName("uuid")
    var uuid: String,
    @SerializedName("create")
    var create: Int,
    @SerializedName("title")
    var title: String,
    @SerializedName("image")
    var image: String,
    @SerializedName("detail")
    var detail: String
)